/**
 * Singelton Pattern
 * <p>
 * A singelton is a construct which makes an object exist only once in the JVM.
 * This is done by setting the constructor to private and accessing the only
 * instance by a gatekeeper.
 * <p>
 */
/**
 * @author marco.pertegato
 *
 */
package ch.e_laborate.desingpatternsinjava.singelton;