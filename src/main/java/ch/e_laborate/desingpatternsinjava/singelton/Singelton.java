package ch.e_laborate.desingpatternsinjava.singelton;

public class Singelton {

	private static int numberOfInstaces = 0;
	private int callOnGetInstance = 0;

	/*
	 * The singelton is static so it exists only once in the JVM.
	 * It is private so you have no access from outside.
	 * And it is final to make sure it is set only once. The final modifier works
	 * because the instance is created at start within the static block.
	 */
	private static final Singelton singelton;

	/*
	 * At start we create a Singelton
	 */
	static {
		singelton = new Singelton();
	}

	/*
	 * The constructor is private.
	 */
	private Singelton() {
		super();
		numberOfInstaces++;
	}

	/*
	 * You can access the singelton only throw the getInstance function.
	 */
	static Singelton getInstance() {
		singelton.callOnGetInstance++;
		return singelton;
	}

	public int callOnGetInstance() {
		return callOnGetInstance;
	}

	public static int getNumberOfInstaces() {
		return numberOfInstaces;
	}

	@Override
	public int hashCode() {
		return 37;
	}

	@Override
	public boolean equals(Object obj) {
		// we just check if references the same object in memory
		return (this == obj);
	}
}
