package ch.e_laborate.desingpatternsinjava.singelton;

public class LazySingelton {

	private static int numberOfInstaces = 0;
	private int callOnGetInstance = 0;

	/*
	 * The singelton is static so it exists only once in the JVM. And it is private
	 * so you have no access from outside. The additional volatile modifier is
	 * needed to make sure JVM does not caches the singelton.
	 */
	private static volatile LazySingelton singelton;

	/*
	 * You can access the singelton only throw the getInstance function.
	 */
	public static LazySingelton getInstance() {
		if (singelton == null) {
			/*
			 * The synchronized function is only needed once, while crating the instance.
			 * This improves the performance dramatically.
			 */
			synchronized (LazySingelton.class) {
				/*
				 * double-checked locking
				 */
				if (singelton == null) {
					singelton = new LazySingelton();
				}
			}
		}
		singelton.callOnGetInstance++;
		return singelton;
	}

	/*
	 * The constructor is private.
	 */
	private LazySingelton() {
		super();
		numberOfInstaces++;
	}

	public int callOnGetInstance() {
		return callOnGetInstance;
	}

	public static int getNumberOfInstaces() {
		return numberOfInstaces;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public boolean equals(Object obj) {
		// we just check if references the same object in memory
		return (this == obj);
	}
}
