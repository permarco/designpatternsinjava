/**
 * Immutable Object Pattern
 * <p>
 * Immutable Objects are read-only object that can not be changed.
 * Therefore they are perfect to share and used by multiple clients.
 * By their nature immutable objects are thread-save.
 * If you need to change an immutable object you have to create a new one.

 */
/**
 * @author marco.pertegato
 *
 */
package ch.e_laborate.desingpatternsinjava.immutable;