package ch.e_laborate.desingpatternsinjava.immutable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * The final in the class makes the class not 
 */
public final class Immutable {

	/*
	 * Fields are marked private so they can not be changed by outside. And they are
	 * marked final because the will be set only once. This works because we set the
	 * fields within the constructor.
	 */
	private final String name;
	private final int number;
	private final List<String> aList;

	/*
	 * All fields are set by the constructor. Afterwards it is not allowed to change the object.
	 * It's clear that any reference to mutable objects needs to be copied. They could change.
	 */
	public Immutable(String name, int number, List<String> aList) {
		this.name = name;
		this.number = number;
		if (aList == null) {
			throw new IllegalArgumentException("the list is required");
		}
		this.aList = new ArrayList<>(aList);
	}

	/*
	 * There are no setters only getter, the fields can not be changed.
	 */
	public String getName() {
		return name;
	}

	public int getNumber() {
		return number;
	}

	/*
	 * Never return a direct reference to a underling mutable object.
	 * You can either return the single elements and the size of the list,
	 * so the client can traverse over the list by himselves.....
	 */
	public int getAListSize() {
		return aList.size();
	}

	public String getListItem(int index) {
		return aList.get(index);
	}

	/*
	 * .... or you can return an object that can not be modified.......
	 */
	public List<String> getAList(){
		return Collections.unmodifiableList(aList);
	}
	
	/*
	 * .... or you could also return a copy of the list.
	 * In any case the client can not change this object.
	 */
	public List<String> getCopyOfAList(){
		return new ArrayList<>(aList);
		
	}
}
