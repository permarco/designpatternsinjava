package ch.e_laborate.desingpatternsinjava.singelton;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class SingeltonTest {

	@Test
	public void testSingelton() throws Exception {

		// The instance is created on the first Class access
		assertEquals(1, Singelton.getNumberOfInstaces());

		Singelton instance1 = Singelton.getInstance();
		assertEquals(1, Singelton.getNumberOfInstaces());
		assertEquals(1, instance1.callOnGetInstance());

		Singelton instance2 = Singelton.getInstance();
		assertEquals(1, Singelton.getNumberOfInstaces());
		assertEquals(2, instance2.callOnGetInstance());
		assertEquals(instance1, instance2);
	}

	@Test
	public void testLasySingelton() throws Exception {

		// The instance is not created yet. It will be created on the first call
		assertEquals(0, LazySingelton.getNumberOfInstaces());

		LazySingelton lasyInstance1 = LazySingelton.getInstance();
		assertEquals(1, LazySingelton.getNumberOfInstaces());
		assertEquals(1, lasyInstance1.callOnGetInstance());

		LazySingelton lasyInstance2 = LazySingelton.getInstance();
		assertEquals(1, LazySingelton.getNumberOfInstaces());
		assertEquals(2, lasyInstance2.callOnGetInstance());
		assertEquals(lasyInstance1, lasyInstance2);
	}

}
