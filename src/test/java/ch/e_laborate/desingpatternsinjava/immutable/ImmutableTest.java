package ch.e_laborate.desingpatternsinjava.immutable;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class ImmutableTest {
	
	Immutable immutable = new Immutable("first object", 1, Arrays.asList("item one","item two"));
	
	@Test
	public void testImmutable() {
		
		assertEquals(1, immutable.getNumber());
		assertEquals("first object", immutable.getName());
		assertEquals(Arrays.asList("item one","item two"), immutable.getAList());
	}
	
	@Test(expected = UnsupportedOperationException.class)
	public void testUnmutability() {
		
		List<String> copy = immutable.getAList();
		assertEquals(copy, immutable.getAList());
		copy.set(1, "change");
	}
	
}
